'use strict';

var  express = require('express'),
 path = require('path'),
//  cookieParser = require('cookie-parser'),
 bodyParser= require('body-parser')


var api1 = require('./controllers/api_v1');
const api2 = require('./controllers/api_v2');

const app = module.exports = express();

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.post('/o',  function(req, res) {
  // Access the provided 'page' and 'limt' query parameters
  let page = req.body;
  // let articles = await Article.findAll().paginate({page: page, limit: limit}).exec();
  // Return the articles to the rendering engine
  console.log(req)
  res.send(req.body);
});



// 添加普通路由
app.use('/api/mail', api1);

// 添加异步路由
app.use('/api/v2', api2);

app.get('/', (req, res) => {
  res.send('Hello form root route.');
});

app.use((err, req, res, next) => {
  const error = (req.user || {}).message + err.message;
  res.send(`${error}<br/>catch a error.`);
});

if (!module.parent) {
  app.listen(3000);
  console.log('Express started on port 3000');
}